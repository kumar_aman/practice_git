from errbot import botflow, FlowRoot, BotFlow, FLOW_END

class MeetingFlows(BotFlow):
        """ Meeting flows"""

            @botflow
                def meeting(self, flow: FlowRoot):

                            meeting = flow.connect('startmeeting', auto_trigger=True)

                                    topic = meeting.connect('topic')
                                            topic.connect(topic)

                                                    action = topic.connect('action')
                                                            action.connect(action)

                                                                    action.connect(topic)

                                                                            endmeeting = topic.connect('endmeeting')
                                                                                    action.connect(endmeeting)
                                                                                            endmeeting.connect(FLOW_END)

