from errbot import botcmd, BotPlugin, botmatch


class Meetingbot(BotPlugin):
        @botcmd
            def startmeeting(self, msg, _):
                        msg.ctx['topics'] = {}
                                msg.ctx['current_topic'] = None
                                        return 'Starting meeting'

                                        @botcmd
                                            def topic(self, msg, args):
                                                        msg.ctx['current_topic'] = args
                                                                msg.ctx['topics'][args] = {'action': [], 'info': []}
                                                                        return 'topic: ' + args

                                                                        @botcmd
                                                                            def action(self, msg, args):
                                                                                        current_topic = msg.ctx['current_topic']
                                                                                                msg.ctx['topics'][current_topic]['action'].append(args)
                                                                                                        return 'action: ' + args

                                                                                                        @botcmd(template="meeting", flow_only=True)
                                                                                                            def endmeeting(self, msg, match):
                                                                                                                        return {'topics': msg.ctx['topics']}

